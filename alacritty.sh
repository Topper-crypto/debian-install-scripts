#!/bin/bash

# this script will install alacritty with cargo.
# this script will also add .cargo and .rustup to your home directory.

# use doas if installed
[ -x "$(command -v doas)" ] && [ -e /etc/doas.conf ] && ld="doas"
[ -x "$(command -v sudo)" ] && ld="sudo"

# install curl, cmake, and pkg-config if not installed
[ -x "$(command -v curl)" ] || $ld apt install curl -yy
[ -x "$(command -v cmake)" ] || $ld apt install cmake -yy
[ -x "$(command -v pkg-config)" ] || $ld apt install pkg-config -yy

#install cargo and rust
curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh

source $HOME/.cargo/env

# install libraries and python 3
$ld apt-get install libfreetype6-dev libfontconfig1-dev libxcb-xfixes0-dev libxkbcommon-dev python3 -yy

cargo install alacritty && echo "alacritty successfully installed"

ln -sf $HOME/.cargo/bin/alacritty $HOME/.local/bin/

echo "alacritty binary is located in ~/.cargo/bin/"

echo "the binary has been symlinked to ~/.local/bin"

echo "try typing alacritty in this terminal, if it launches, you're golden,"

echo "if not, symlink or move the binary to /usr/local/bin/  and try again."





