#!/bin/bash
# use doas if installed

[ -x "$(command -v doas)" ] && [ -e /etc/doas.conf ] && ld="doas"
[ -x "$(command -v sudo)" ] && ld="sudo"

# Install headers for  your kernel
$ld apt install linux-headers-"$(uname -r)" linux-image-amd64 -yy

# Install ZFS DKMS and ZFS utilities
$ld apt install zfs-dkms zfsutils-linux -yy

# Add the kernel Modules
$ld modprobe zfs

# Enable ZFS services
$ld systemctl enable zfs.target || $ld rc-update add zfs.target default
$ld systemctl enable zfs-import-cache || $ld rc-update add zfs-import-cache default
$ld systemctl enable zfs-mount || $ld rc-update add zfs-mount default
$ld systemctl enable zfs-import.target || $ld rc-update add zfs-import.target default
$ld systemctl enable zfs-import-scan || $ld rc-update add zfs-import-scan default
$ld systemctl enable zfs-share || $ld rc-update add zfs-share default

# Restart all ZFS services

$ld systemctl restart zfs-import-cache || $ld rc-service zfs-import-cache restart
$ld systemctl restart zfs-import-scan || $ld rc-service zfs-import-scan restart
$ld systemctl restart zfs-mount || $ld rc-service zfs-mount restart
$ld systemctl restart zfs-share || $ld rc-service zfs-share restart
